#include "List.h"
#include <iostream>


List::~List() { 
	for (Node* p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::headPush(int num) { //head push function
	Node* h = new Node(num);
	if (head==0) {
		tail = h;
	}
	h->next = head;
	head = h;
}

void List::display() { //display function
	Node* dp = head;
	while (dp != 0) {
		cout<<dp->info;
		dp = dp->next;
		cout << " ";
	}
}

void List::tailPush(int num) { //tail push function
	Node* t = new Node(num);
	tail->next = t;
	tail = t;
}

int List::headPop() { //head pop function
	Node* n = head;
	int store = head->info; //store value before delete
	head = head->next;
	delete n;
	return store;
}

int List::tailPop() { //tail pop function
	Node* p = head;
	int store = tail->info; //store value before delete
	while (p->next != tail) {
		p = p->next;
	}
	delete tail;
	p->next = 0;
	tail = p;
	return store;
}

void List::deleteNode(int num) { //delete function
	Node* d = head, *d1,*d2;
	while (d->next->info != num) {
		d = d->next;//point to the previous node you want to delete
	}

	d1 = d->next; //point to the node you want to delete
	d2 = d1->next;//point to the next node you want to delete
	delete d1;
	d->next = d2;
}

bool List::isInList(int num) { //fund list function
	Node* f = head;

	while (f != 0) {
		if (f->info == num) {
			return true;
		}
		else {
			f = f->next;
		}
	}

	return false;
}